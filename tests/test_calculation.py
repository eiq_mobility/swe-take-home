import logging
from unittest.mock import patch, Mock
import pytest
from swe_take_home.calculation import start_calculation

# mock time.sleep, _simulate_error, test red, green and blue functions and confirm it sleeps.
# call it 100 times and confirm that it errors roughly as expected

def run_calculation(foo, bar, baz):
    result = None
    error = None

    def on_result(r):
        nonlocal result
        result = r

    def on_error(e):
        nonlocal error
        error = e

    with patch('swe_take_home.calculation.time.sleep', Mock()):
        start_calculation(foo, bar, baz, on_result, on_error)

    return result, error


epsilon = 0.0001


@pytest.mark.parametrize('foo,bar,baz,expected', [
    ('red', 12.5, 4, 6.5257),
    ('green', 12.5, 4, 3.9336),
    ('blue', 12.5, 4, 4.9977)
])
def test_completed_calculation(foo, bar, baz, expected):
    with patch('swe_take_home.calculation._simulate_error', Mock(return_value=None)) as sleep:
        result, error = run_calculation(foo, bar, baz)
        sleep.assert_called_once()
        assert error is None
        assert abs(result - expected) < epsilon


def test_simulated_errors():
    foo = "red"
    bar = 12.5
    baz = 4
    results = [run_calculation(foo, bar, baz) for _ in range(100)]
    errors = [error for _, error in results if error]
    # just make sure it's producing errors roughly as often as expected
    assert 20 < len(errors) < 40
