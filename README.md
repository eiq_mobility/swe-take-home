# eIQ Mobility SWE Take-Home Assignment

Hello! Thank you for taking the time to do this take-home
assignment. We hope you enjoy working on it.

We'd like you to

1) Develop a small webapp server, in Python 3, using:

- the [flask](https://flask.palletsprojects.com/en/2.1.x/) web framework,
- the [flask-smorest](https://flask-smorest.readthedocs.io/en/latest/) REST API definition framework,
- the [SQLAlchemy](https://docs.sqlalchemy.org/en/14/) database mapping library,
- any other libraries you find useful in implementing this.

2) Write a [Dockerfile](https://docs.docker.com/) that runs the app
in a container and responds to requests on port `8080`.

The server you'll implement runs long-running calculations
(simulated).  The file `./swe_take_home/calculation.py` provides a
function, `start_calculation` that simulates a long-running calculation
that takes between five and 30 seconds to produce a
numeric result. The HTTP server you'll implement lets clients start
new calculations and query for their results.

To constrain the scope of the assignment, the app does not
concern itself with users or authentication/authorization.
Any client can call the routes.

You can use an in-memory
[SQLite](https://docs.sqlalchemy.org/en/14/dialects/sqlite.html)
database and have SQLAlchemy [create the tables on app
startup](https://docs.sqlalchemy.org/en/14/tutorial/metadata.html#id1).

## Installation

To get started, if you don't already have it installed, install
the [poetry](https://python-poetry.org/) python package manager tool.

```
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

Then, in this directory, install this project's dependencies:
```
$ poetry install
```

Confirm that the tests run:
```
$ poetry run pytest
```

You should see output like the following:
```
================================= test session starts =================================
platform darwin -- Python 3.10.4, pytest-7.1.2, pluggy-1.0.0
collected 1 item

tests/test_calculation.py ....                                                  [100%]

================================== 1 passed in 0.04s ==================================
```

If so, you should be good to go.

## Running your server

Make the main entrypoint a top-level file in this directory called
`app.py`. You can then start it with

```
poetry run ./app.py
```

Good luck!

## The app

The app you'll implement exposes three routes:

### POST /calculations

Starts a new calculation running.

The POST body is json of the form:

```
{
  "calculation_type": a string, one of "red", "green" or "blue",
  "x_value": any floating-point number,
  "y_translation": an integer between -10 and 10, inclusive at both ends of the range
}
```

The route should return HTTP code 400 if the inputs aren't valid according to that definition,
with a JSON response body of the form
```
{
  "error": "< the validation error >"
}
```

The route handler does the following:
1) In a new thread or process, start a calculation by calling `start_calculation` defined in
`./swe_take_home/calculation.py`,
2) record the started calculation in the database, and
3) return HTTP code `201 Created`, and a JSON response body of the form
```
{
  "id": "<a UUID>"
}
```

where `<a UUID>` is a UUID uniquely identifying the calculation that the
post request started.

#### The calculation runs asynchronously

Note that the request should return its response to the client as soon
as it has started the calculation and recorded the new calculation in
the database; it should not wait for the calculation to finish to
return its response.

Instead, it should start the calculation in a new process or thread
and let it run to completion. The callbacks that the route handler
provides to `start_calculation` will be called when the calculation
completes or errors, letting them record the result or error in the
database to be available for subsequent client queries.

### GET /calculations

Returns a list of currently running calculations and calculations that
have recently completed.

Accepts an optional query var `since`, a number of seconds, specifying
how old the completed calculations in the returned list can be. So
specifying `since=60` will return all running calculations and all
calculations that completed within the last 60 seconds. Omitting `since`
returns all completed calculations along with all running calculations.

Each calculation in the returned list is of the form
```
{
  "id": a string,
  "calculation_type": the `calculation_type` value submitted to start the calculation,
  "x_value": the `x_value` value submitted to start the calculation,
  "y_translation": the `y_translation` value submitted to start the calculation,
  "started_at": a timestamp string designating when the calculation started,
  "result": (optional) {
    "completed_at": a timestamp string designating when the calculation completed,
    "result": a floating-point number
  },
  "error": (optional) {
    "errored_at": a timestamp string designating when the calculation encountered an error,
    "message": a string
  }
}
```

Each calculation will contain either a `result` or an `error` object, but never both.

### GET /calculations/<id>

Returns JSON of the form given above for the calculation whose ID is
`<id>`.

## Running calculations

The `POST /calculations` route calls `start_calculation` defined in
`calculation.py` with the `calculation_type`, `x_value` and
`y_translation` inputs provided by the client.  `start_calculation`
also takes two callback arguments, `on_completed` and `on_error`, that
it calls when the calculation completes or errors, respectively.

The callbacks you provide to `on_completed` and `on_error` should
update the calculation in the database, setting the `completed_at` or
`errored_at` timestamp. The `on_error` callback should also set the
`error` value of the calculation in the database.

## Docker

When the server is finished, write a
[Dockerfile](https://docs.docker.com/) that runs the app and has it
respond to requests on port `8080`.

## Good luck!

Thanks again for the time you're giving us in implementing this
assignment. We look forward to reviewing your project and meeting you!
